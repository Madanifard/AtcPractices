package com.example.amirreza.actpractices.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.example.amirreza.actpractices.Icode;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;

import cz.msebera.android.httpclient.Header;

/**
 * Created by amirreza on 11/17/17.
 */

public class DownloaderService extends Service {

    /**
     * stopSelf(); => for stop Service
     */
    String url;
    @Override
    public void onCreate() {
        super.onCreate();

        Icode.ShowShortToast(this, "onCreate DownloaderService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Icode.ShowShortToast(this, "onStartCommand DownloaderService");

        url = intent.getStringExtra("url");

        Icode.ShowShortToast(this, url);
        downloadFile(url);

        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void downloadFile(String url) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(this) {
            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // bytesWritten from totalSize
                super.onProgress(bytesWritten, totalSize);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                // file.getAbsoluteFile(); => get place file Download

                // file.renameTo(AddressFile); after get file must move other place file
            }
        });
    }
}
