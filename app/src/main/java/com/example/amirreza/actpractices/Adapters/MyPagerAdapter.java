package com.example.amirreza.actpractices.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.amirreza.actpractices.Fragments.fragment_tab_1;
import com.example.amirreza.actpractices.Fragments.fragment_tab_2;
import com.example.amirreza.actpractices.Fragments.fragment_tab_3;
import com.example.amirreza.actpractices.R;

/**
 * Created by amirreza on 11/17/17.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {

    public Context mContext;

    public MyPagerAdapter(FragmentManager fm, Context mContext) {
        super(fm);
        this.mContext = mContext;
    }

    @Override
    public Fragment getItem(int position) {

        if(position == 0) {
            return fragment_tab_1.getInstant();
        } else if(position == 1) {
            return fragment_tab_2.getInstance();
        } else if(position == 2) {
            return fragment_tab_3.getInstance();
        } else {
            return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if(position == 0) {
            return "fragment_tab_1";
        } else if(position == 1) {
            return "fragment_tab_2";
        } else if(position == 2) {
            return "fragment_tab_3 " + mContext.getString(R.string.Hello);
        } else {
            return null;
        }

    }
}
