package com.example.amirreza.actpractices;

import android.content.Context;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * Created by amirreza on 10/6/17.
 */

public class Icode {

    /**
     * storeSandBox
     * @param mContext
     * @param key
     * @param value
     */
    public static void storeSandBox(Context mContext, String key, String value) {

        PreferenceManager.getDefaultSharedPreferences(mContext).
                edit().
                putString(key, value).
                apply();
    }

    /**
     * getSandBox
     * @param mContext
     * @param key
     * @return
     */
    public static String getSandBox(Context mContext, String key) {

        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, "Not Set Any thing");
    }

    /**
     * show the short toast
     * @param context
     * @param message
     */
    public static void ShowShortToast(Context context, String message) {

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
