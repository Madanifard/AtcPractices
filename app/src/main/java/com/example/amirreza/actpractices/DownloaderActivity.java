package com.example.amirreza.actpractices;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.amirreza.actpractices.Service.DownloaderService;

public class DownloaderActivity extends BaseActivity {


    EditText url;
    Button btnDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloader);

        //for get Permission form User
        if (ContextCompat.checkSelfPermission(mContent,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1500);
        }

        bindWidget();

    }

    public void bindWidget() {

        url = findViewById(R.id.et_url);
        btnDownload = findViewById(R.id.btn_download);
    }


    /**
     * start service
     * @param view
     */
    public void onClickDownload(View view) {

        Intent intentDownloader = new Intent(mContent, DownloaderService.class);
        intentDownloader.putExtra("url", url.getText().toString()); // set url
        startService(intentDownloader);
    }
}
