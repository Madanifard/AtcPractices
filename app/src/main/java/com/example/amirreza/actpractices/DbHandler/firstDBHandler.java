package com.example.amirreza.actpractices.DbHandler;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.jar.Attributes;

/**
 * Created by amirreza on 10/27/17.
 */

public class firstDBHandler extends SQLiteOpenHelper {

    String create_table_users = "CREATE TABLE users (" +
            "_id INTEGER AUTO INCREMENT PRIMARY KEY ," +
            "name TEXT" +
            "family TEXT" +
            ")";

    public firstDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(create_table_users);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
     * get data and insert into database
     * @param name
     * @param family
     */
    public void insertUser(String name, String family) {

        String insertCommand = "INSERT INTO users (name, family) VALUES(" +
                "'" + name + "'" +
                "'" + family + "'" +
                ")";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(insertCommand);
        db.close();

    }

    /**
     * for read all data
     * @return
     */
    public String ReadAllData() {

        String result = "";
        String selectQuery = "SELECT name, family FROM users";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        while (cursor.moveToNext()) {
            result += cursor.getString(0) + " " + cursor.getString(1) + "\n";
        }

        return result;
    }

}
