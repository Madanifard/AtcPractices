package com.example.amirreza.actpractices.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.amirreza.actpractices.Icode;
import com.example.amirreza.actpractices.R;

/**
 * Created by amirreza on 11/17/17.
 */

public class fragmentA extends Fragment implements View.OnClickListener {

    Button btnFragmentClick;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_a, container, false);
        //create Lagik widgets

        bindWidget(view);

        // solution 1 for get Event Click
        btnFragmentClick.setOnClickListener(this);


        return view;
//        return super.onCreateView(inflater, container, savedInstanceState);
    }

    // solution 1 for get Event Click
    @Override
    public void onClick(View view) {

        Icode.ShowShortToast(getContext(), "fire in fragment Class A");
    }

    /**
     * for bind to view
     * @param view
     */
    public void bindWidget(View view) {

        btnFragmentClick = view.findViewById(R.id.btn_fragment_click);
    }
}
