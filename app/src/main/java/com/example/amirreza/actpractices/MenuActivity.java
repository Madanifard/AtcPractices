package com.example.amirreza.actpractices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MenuActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

    }

    /**
     * create menu
     * @param featureId
     * @param menu
     * @return
     */
    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {

        menu.add(0, 100, 1, getString(R.string.settings));
        menu.add(0, 200, 2, "First Activity");
        menu.add(0, 300, 3, "Exit");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return super.onCreatePanelMenu(featureId, menu);

    }

    /**
     * for handel the click on menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == 100) {

            Icode.ShowShortToast(mContent, "Click the Settings");

        } else if(item.getItemId() == 200) {

            Intent intent = new Intent(mContent, MainActivity.class);
            startActivity(intent);

        } else if(item.getItemId() == 300) {

            finish();
        } else if(item.getItemId() == R.id.menu1) {

            Icode.ShowShortToast(mContent, "Click the Mnu 1");

        } else if(item.getItemId() == R.id.menu2) {

            Icode.ShowShortToast(mContent, "Click the Mnu 2");
        }

        return super.onOptionsItemSelected(item);
    }
}
