package com.example.amirreza.actpractices;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.amirreza.actpractices.DbHandler.firstDBHandler;



public class DataBaseActivity extends BaseActivity {

    EditText name, family;
    TextView resultDB;

    firstDBHandler userDB ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_data_base);

        bindWidget();

        userDB = new firstDBHandler(mContent, "atc.db", null, 1);

    }

    /**
     * binding widget
     */
    public void bindWidget() {

        name = (EditText) findViewById(R.id.edit_db_name);
        family = (EditText) findViewById(R.id.edit_db_family);
        resultDB = (TextView) findViewById(R.id.db_result);
    }

    /**
     * Onclick event for insert to database
     * @param view
     */
    public void insertUserDB(View view) {

        userDB.insertUser(name.getText().toString(), family.getText().toString());

        Icode.ShowShortToast(mContent, "Data insert");
    }

    /**
     * for read data
     * @param view
     */
    public void readUserDB(View view) {

        String result = userDB.ReadAllData();

        resultDB.setText(result);
    }

}
