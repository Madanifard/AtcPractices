package com.example.amirreza.actpractices.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by amirreza on 11/3/17.
 *
 * this way when program is close , if doing event this class get Event,
 * if devise restart this program run
 *
 * if device boot , if write the code in manifest:
 *
 * <receiver android:name="MyScheduleReceiver" >
     <intent-filter>
     <action android:name="android.intent.action.BOOT_COMPLETED" />
     </intent-filter>
   </receiver>
 *
 * if use in android < 6 must add permission:
 * <uses-permission android:name="android.permission.READ_PHONE_STATE" />
 *
 */

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

    }
}
