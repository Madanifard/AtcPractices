package com.example.amirreza.actpractices.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.amirreza.actpractices.R;

/**
 * Created by amirreza on 10/19/17.
 */

public class StudentListAdapters extends BaseAdapter {

    Context mContext;

    String nameStdudent[];

    public StudentListAdapters(Context mContext, String[] nameStdudent) {

        this.mContext = mContext;
        this.nameStdudent = nameStdudent;
    }

    @Override
    public int getCount() {
        return nameStdudent.length;
    }

    @Override
    public Object getItem(int position) {
        return nameStdudent[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View rowView = LayoutInflater.from(mContext).inflate(R.layout.student_list_item, viewGroup, false);

        TextView student_name = (TextView) rowView.findViewById(R.id.student_name); //get widget from student_list_item

        student_name.setText(nameStdudent[position]);

        return rowView;
    }
}
