package com.example.amirreza.actpractices;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegistryFormActivity extends AppCompatActivity {

    EditText editTxtName, editTxtFamily, editTxtAge, editTxtEmail;
    Button btnSubmit;
    TextView txtViewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry_form);

        getWidget();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String message = "نام:‌ " + editTxtName.getText().toString() +
                        " نام خوانوادگی: " + editTxtFamily.getText().toString() +
                        " سن: " + editTxtAge.getText().toString() +
                        " ایمیل: " + editTxtEmail.getText().toString();
                txtViewMessage.setText(message);
                txtViewMessage.setTextColor(Color.GREEN);
            }
        });
    }

    /**
     * get all widget form xml
     */
    public void getWidget() {

        editTxtName = (EditText) findViewById(R.id.edit_txt_name);
        editTxtFamily = (EditText) findViewById(R.id.edit_txt_family);
        editTxtAge = (EditText) findViewById(R.id.edit_txt_age);
        editTxtEmail = (EditText) findViewById(R.id.edit_txt_email);

        btnSubmit = (Button) findViewById(R.id.btn_submit);

        txtViewMessage = (TextView) findViewById(R.id.txt_view_message);

    }
}
