package com.example.amirreza.actpractices.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.amirreza.actpractices.Icode;

/**
 * Created by amirreza on 11/3/17.
 */

public class IncomingCallReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        Icode.ShowShortToast(context, "Run Receiver incoming Call in ATC project");
    }
}
