package com.example.amirreza.actpractices;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageActivity extends BaseActivity implements View.OnClickListener  {

    ImageView loadImage, imgUrlLoad;
    Button btnShow, btnLoadImage;
    EditText txtUrlImage;

    String urlLoadImage = "http://farm7.staticflickr.com/6057/6233529135_84914afb62.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image);

        widget();
        
        setInitialWidget();
        
        setClick();
    }

    /**
     * this function for set initial widgets
     */
    private void setInitialWidget() {

        txtUrlImage.setText(urlLoadImage);
    }

    public void widget() {

        loadImage = (ImageView) findViewById(R.id.loadImage);
        btnShow = (Button) findViewById(R.id.btnShow);

        btnLoadImage = (Button) findViewById(R.id.btn_load_image);
        txtUrlImage = (EditText) findViewById(R.id.txt_url_image);
        imgUrlLoad = (ImageView) findViewById(R.id.img_url_load); 
    }

    /**
     * this method for set the click
     */
    public void setClick() {

        btnShow.setOnClickListener(this);
        btnLoadImage.setOnClickListener(this);
    }

    /**
     * this onclick for all widget click
     * @param view
     */
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnShow :
                setImage();
                Icode.ShowShortToast(mContent, "image set!");
                break;

            case R.id.btn_load_image :
                setImagePicasso();
                Icode.ShowShortToast(mContent, "Picasso load Image");
                break;

        }

    }

    /**
     * for set the image
     */
    public void setImage() {

        loadImage.setImageResource(R.mipmap.images);

    }

    /**
     * get image url and load with Picasso
     */
    public void setImagePicasso() {

        urlLoadImage = txtUrlImage.getText().toString();

        Picasso.with(mContent).load(urlLoadImage).into(imgUrlLoad);

    }

}
