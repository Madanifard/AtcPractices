package com.example.amirreza.actpractices;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.amirreza.actpractices.Adapters.StudentListAdapters;

public class MyListViewActivity extends BaseActivity {

    ListView lstList ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_list_view);

        widget();

        String name[] = { "Amirreza", "Amir", "Reza"};

        StudentListAdapters adapter = new StudentListAdapters(mContent, name);

        lstList.setAdapter(adapter); // for joined adapter

        lstList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                String clickItem = (String) adapterView.getItemAtPosition(position);
                Icode.ShowShortToast(mContent, clickItem);
            }
        });
    }

    public void widget() {

        lstList = (ListView) findViewById(R.id.lst_list);

    }
}
