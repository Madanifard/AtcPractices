package com.example.amirreza.actpractices;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class LocationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);


        /**
         * first step get Primary Permission
         */
        if (ContextCompat.checkSelfPermission(mContent, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1500);
        }

        if (ContextCompat.checkSelfPermission(mContent, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    1500);
        }


        //use from Smart Location Library
        SmartLocation.with(mContent).location()
                .start(new OnLocationUpdatedListener(){

                    @Override
                    public void onLocationUpdated(Location location) {
                        //when change the location get new Location

//                        location.getLatitude();
//                        location.getLongitude();

                    }
                });

    }
}
