package com.example.amirreza.actpractices;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;


public class SandBoxActivity extends BaseActivity {

    TextView show, showHawk;
    EditText name, family;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sand_box);

        Hawk.init(mContent).build();

        getWidget();
    }

    /**
     * get widget
     */
    public void getWidget() {

        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        show = (TextView) findViewById(R.id.show);
        showHawk = (TextView) findViewById(R.id.showHawk);

    }

    /**
     * for store the data in SandBox
     * @param view
     */
    public void storeInSandBox(View view) {

        Icode.storeSandBox(mContent, "name", name.getText().toString());
        Icode.storeSandBox(mContent, "family", family.getText().toString());

        Icode.ShowShortToast(mContent, "Name and Family Store!!");
    }

    /**
     * for get from sandBox
     * @param view
     */
    public void getFromSandBox(View view) {

        String result = Icode.getSandBox(mContent, "name") + " " +
                Icode.getSandBox(mContent, "family");

        show.setText(result);

        Icode.ShowShortToast(mContent, "Get!!");
    }

    /**
     * this method for set the new Data in sandBox
     * https://github.com/orhanobut/hawk
     * @param view
     */
    public void storeHawk(View view) {

        Hawk.put("name_hawk", name.getText().toString());
        Hawk.put("family_hawk", family.getText().toString());

        Icode.ShowShortToast(mContent, "Name and Family Store with Hawk!!");
    }

    /**
     * for gwt Data And Show in TextView
     * https://github.com/orhanobut/hawk
     * @param view
     */
    public void getHawk(View view) {

        String data = Hawk.get("name_hawk", "Not Set") + " " +
                Hawk.get("family_hawk", "Not Set");

        showHawk.setText(data);

        Icode.ShowShortToast(mContent, "Get with Hawk!!");
    }

    /**
     * for Delete All Hawk
     * https://github.com/orhanobut/hawk
     * @param view
     */
    public void deleteAllHawk(View view) {

        Hawk.deleteAll();

        Icode.ShowShortToast(mContent, "Delete All Key name and family !!");
    }
}
