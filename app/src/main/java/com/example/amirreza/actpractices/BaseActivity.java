package com.example.amirreza.actpractices;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by amirreza on 10/6/17.
 */

public class BaseActivity extends AppCompatActivity {

    public Context mContent = this;
    public Activity mActivity = this;
}
