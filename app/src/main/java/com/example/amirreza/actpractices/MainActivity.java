package com.example.amirreza.actpractices;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends BaseActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

    }

    /**
     * this function for redirect to registry form
     */
    public void sendToRegistryForm(View view) {

        Intent intent = new Intent(mContent, RegistryFormActivity.class);
        startActivity(intent);
    }


    /**
     * for load the sheared Data
     * @param view
     */
    public void sendToSandBox(View view) {

        Intent intent = new Intent(mContent, SandBoxActivity.class);
        startActivity(intent);
    }

    /**
     * for load the image page
     * @param view
     */
    public void sendToImage(View view) {

        Intent intent = new Intent(mContent, ImageActivity.class);
        startActivity(intent);
    }

    /**
     * for load the listView
     * @param view
     */
    public void sendToListView(View view) {

        Intent intent = new Intent(mContent, MyListViewActivity.class);
        startActivity(intent);

    }

    /**
     * for send to WebServices
     * @param view
     */
    public void sendToWebService(View view) {

        Intent intent = new Intent(mContent, WebServicesActivity.class);
        startActivity(intent);
    }
}
