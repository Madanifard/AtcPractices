package com.example.amirreza.actpractices;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.amirreza.actpractices.Models.WeatherApiModel.WeatherModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

public class WebServicesActivity extends BaseActivity {

    Button btnSearch;
    EditText editCityName;
    TextView txtResult;

    String cityName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web_services);

        widget();
    }

    /**
     * Bind the widgets
     */
    public void widget() {

        btnSearch = (Button) findViewById(R.id.btn_search);
        editCityName = (EditText) findViewById(R.id.edit_city_name);
        txtResult = (TextView) findViewById(R.id.txt_result);
    }

    /**
     * when click search
     * @param view
     */
    public void searchCityWeather(View view) {

        cityName = editCityName.getText().toString();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                getWeather(cityName);
//            }
//        }).start();

        getWeatherAsyncHttpClient(cityName);
    }

    /**
     * get data from Api with AsyncHttpClient => http://loopj.com/android-async-http/
     * @param city_name
     */
    public void getWeatherAsyncHttpClient(String city_name) {

        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                city_name +
                "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() { // because have the Text Response
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                //if create Error  when connect to the server
                Icode.ShowShortToast(mContent, throwable.toString());

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                parseJsonGson(responseString);

            }

        });
    }


    /**
     * get Url and Get weather information
     * @param city_name
     */
    public void getWeather(String city_name) {

        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                city_name +
                "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

        try {

            URL obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");

            int responseCode = con.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String inputLine;

                final StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {

                    response.append(inputLine);

                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        txtResult.setText( parseJason(response.toString()) ); // set to OutPut
                        txtResult.setText( parseJsonGson(response.toString()) ); // set to OutPut with Json
                    }
                });

            }

        } catch (Exception ex) {

            Log.d("Error", "getWeather:WepService:", ex);

        }
    }

    /**
     * parse json from Gson
     *
     * @param server_response
     * @return
     */
    public String parseJsonGson(String server_response) {

        //http://www.jsonschema2pojo.org/ => for create the Model

        String result = "Not Set Any Thing";

        Gson gson = new Gson();
        WeatherModel weather = gson.fromJson(server_response, WeatherModel.class);

        if(weather.getQuery().getCount() >= 1) { // this part for check status code

            String tempercher = weather.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
            String sate = weather.getQuery().getResults().getChannel().getItem().getCondition().getText();
            String toDay = weather.getQuery().getResults().getChannel().getItem().getCondition().getDate();

            result = "tempercher : " + tempercher + " F " + "\n" +
                    "Sate : " + sate + "\n" +
                    "to day : " + toDay;

        }


        return result;
    }

    /**
     * for parse Json with main way
     * @param server_response
     * @return
     */
    public String parseJason(String server_response) {

        String result = "Not Set Any Thing";

        try {

            JSONObject allObj = new JSONObject(server_response);

            String queryStr = allObj.getString("query");
            JSONObject queryObj = new JSONObject(queryStr);

            String resultStr = queryObj.getString("result");
            JSONObject resultObj = new JSONObject(resultStr);

            String channelStr = resultObj.getString("channel");
            JSONObject channelObj = new JSONObject(channelStr);

            String itemStr = channelObj.getString("item");
            JSONObject itemObj = new JSONObject(itemStr);

            String conditionStr = itemObj.getString("condition");
            JSONObject conditionObj = new JSONObject(conditionStr);

            String temp = conditionObj.getString("temp");
            String state = conditionObj.getString("text");
            String date = conditionObj.getString("date");

            result = "tempercher : " + temp + " F " + "\n" +
                            "Sate : " + state + "\n" +
                            "to day : " + date;



        } catch (Exception ex) {

            Log.d("Error", "getWeather:WepService:", ex);

        } finally {

            return result;
        }
    }

}
