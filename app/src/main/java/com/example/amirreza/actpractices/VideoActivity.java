package com.example.amirreza.actpractices;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoActivity extends BaseActivity {

    VideoView video;
    String url;
    BroadcastReceiver callReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        bindWidget();

        url = "https://hw20.asset.aparat.com/aparat-video/5b9f9470216e088f08416b433abb02e18551807-144p__56884.mp4";

        //check the READ_PHONE_STATE is have permission or not,
        //if not get permission from user
        if (ContextCompat.checkSelfPermission(VideoActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(VideoActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    1500);
        }

        setUrlToVideo(url);

        createBroadCastReceiver();
    }

    public void bindWidget() {

        video = (VideoView) findViewById(R.id.video_1);
    }

    /**
     * for set the url to my video
     * @param url
     */
    public void setUrlToVideo(String url) {

        video.setMediaController(new MediaController(mContent));
        video.setVideoURI(Uri.parse(url));

        video.start();
    }

    /**
     * this method run after show message for get permission , for tow model if take permission or not
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 1500) {
            // click in ActivityCompat.requestPermissions
            Icode.ShowShortToast(mContent, "Result in Permission");

        }
    }

    /**
     * handel if coming call when playing the video
     */
    public void createBroadCastReceiver() {

        callReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(video.isPlaying()) {
                    video.pause();
                }
            }
        };

        IntentFilter callFilter = new IntentFilter("android.intent.action.PHONE_STATE");

        registerReceiver(callReceiver, callFilter); // for connected the receiver and filter together

        //then added unregisterReceiver in onDestroy function
    }

    /**
     * for delete receiver when activity Destroy
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(callReceiver);
    }
}
