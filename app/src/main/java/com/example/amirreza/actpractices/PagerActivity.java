package com.example.amirreza.actpractices;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.amirreza.actpractices.Adapters.MyPagerAdapter;

public class PagerActivity extends BaseActivity {

    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);

        bindWidget();
    }

    /**
     * bind widgets
     */
    public void bindWidget() {

        pager = findViewById(R.id.pager1);

        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager(), mContent);

        pager.setAdapter(adapter);
    }
}
